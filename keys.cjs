const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

function keys(obj) {
    let keys = []
    for (const key in obj) {
        keys.push(key);
    }
    return keys
}

console.log(keys(testObject))

module.exports = keys
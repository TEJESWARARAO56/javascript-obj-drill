const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

function pairs(obj) {
    let pairs = []
    for (const key in obj) {
        pairs.push([key, obj[key]])
    }
    return pairs
}

console.log(pairs(testObject))

module.exports = pairs
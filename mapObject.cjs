const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

function cb(i) {
    if (typeof i === "string") {
        return i + "hii"
    } else {
        return i * 2
    }
}

function mapObject(obj, cb) {
    for (const key in obj) {
        obj[key] = cb(obj[key])
    }
    return obj
}
mapObject(testObject, cb)
console.log(testObject)

module.exports = mapObject
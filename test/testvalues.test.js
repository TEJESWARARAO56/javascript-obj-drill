let get_values = require('../values.cjs')

test('checking default obj function', () => {
    const result = get_values({ satish: 24, ravi: 21, kill: 13 });
    expect(result).toStrictEqual([24, 21, 13]);
});
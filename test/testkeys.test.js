let getkeys = require('../keys.cjs')

test('checking default obj function', () => {
    const result = getkeys({ name: 'Bruce Wayne', age: 36, location: 'Gotham' });
    expect(result).toStrictEqual(["name", "age", "location"]);
});
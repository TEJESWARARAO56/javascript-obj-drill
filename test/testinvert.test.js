let invert=require('../invert.cjs')

test('checking default obj function', () => {
  const result = invert({"name":"Tejeswar",age:24});
  expect(result).toStrictEqual({"Tejeswar":"name",24:"age"});
});
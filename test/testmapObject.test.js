let map_object = require('../mapObject.cjs')

function cb(number) {
    return number * 4
}

test('checking default obj function', () => {
    const result = map_object({ satish: 24, ravi: 21, kill: 13 },cb);
    expect(result).toStrictEqual({ satish: 96, ravi: 84, kill: 52 });
});
let defaults=require('../defaults.cjs')


test('checking default obj function', () => {
  const result = defaults({"name":"Tejeswar"},{age:24,"name":"captain"});
  expect(result).toStrictEqual({"name":"Tejeswar",age:24});
});
let get_pairs = require('../pairs.cjs')

test('checking default obj function', () => {
    const result = get_pairs({ satish: 24, ravi: 21, kill: 13 });
    expect(result).toStrictEqual([["satish",24],["ravi",21],["kill",13]]);
});
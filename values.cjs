const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

function values(obj) {
    let values = []
    for (const key in obj) {
        values.push(obj[key])
    }
    return values
}

console.log(values(testObject))

module.exports = values
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

function invert(obj) {
    let invertedObj = {}
    for (const key in obj) {
        invertedObj[obj[key]] = key
    }
    return invertedObj
}
console.log(invert(testObject))
console.log(testObject)

module.exports = invert

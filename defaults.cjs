const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

function defaults(obj, defaultProps) {
    for (let key in defaultProps) {
        if (obj[key] === undefined) {
            obj[key] = defaultProps[key]
        }
    }
    return obj
}

defaults(testObject, { name: "bye", akk: 89 })

console.log(testObject)

module.exports = defaults
